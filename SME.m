%Script
%Version 1.1
    %Added ISS coordinates
    %Added ISS to state vector
    %Created Array "Positions" to track only the positions for ease of
    %exporting
%Version 1.2
    %Added the variable "Years" to control the number of years, and
    %adjusted N accordingly
    %Added saving to file functionality for the position data
%Version 1.3
    %Added a scaling factor to the ISS rotation and included it in the
    %initialisation of the position vector
    

% Initial coordinates?

%Constructing 18 degree state vector components using initial conditions
pos_earth =[-9.939965630999040E-01; -9.280335886644397E-02 ;-3.372205662036059E-05];
pos_sun = [-1.442040576327698E-03 ;7.562890356166188E-03 ;3.964572244984745E-05];
pos_moon = [-9.943434955886906E-01 ; -9.539384169882230E-02 ; 8.621251671542610E-05];
pos_ISS = [2.077855041069365E-05 ; -3.160372393125322E-05 ; -2.513608315926497E-05];
    
v_earth =[1.436001612087754E-03; -1.717932890410716E-02; 1.498733435012847E-06];
v_sun   =[-8.266230930896278E-06;1.123612057633019E-06;2.152585370677120E-07];
v_moon  = [2.000765639938274E-03;-1.729368637118795E-02;-4.039005718927104E-05];
v_ISS   = [2.910322157164719E-03;3.020954194768213E-03;-1.393385930438952E-03];

vect = [pos_earth;v_earth;pos_sun;v_sun;pos_moon;v_moon]; % combine into 1 vector
vect_ISS = [pos_ISS;v_ISS];

Years = 1;
h = 0.00025; % Step (in days)
N = 365*Years*(1/h); % Number of steps

[Storage,Moon_Phase] = ThreeBodyProblem_RK4(vect,h,N);
Storage_ISS = TwoBodyProblem_RK4(vect_ISS,h,N);

ISS_Distance_Scale = 30;

Storage_ISS = (ISS_Distance_Scale*Storage_ISS(1:3,:))+Storage(1:3,:);

Positions = [Storage(1,:);Storage(2,:);Storage(3,:);Storage(7,:);Storage(8,:);Storage(9,:);Storage(13,:);Storage(14,:);Storage(15,:);Storage_ISS(1,:);Storage_ISS(2,:);Storage_ISS(3,:)];

save Positions Positions
save Moon_Phase Moon_Phase

hold on
plot3(Positions(1,:),Positions(2,:),Positions(3,:))
plot3(Positions(4,:),Positions(5,:),Positions(6,:))
plot3(Positions(7,:),Positions(8,:),Positions(9,:))
plot3(Positions(10,:),Positions(11,:),Positions(12,:))
xlabel("X (AU")
ylabel("Y (AU")
zlabel("Z (AU")
%grid on
legend('Earth','Sun','Moon','ISS')
hold off