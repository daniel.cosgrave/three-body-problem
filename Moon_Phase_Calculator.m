function [Moon_Phase]=Moon_Phase_Calculator(Cycle_Day)
    %Version 1.1
        %Removed floating numbers and gave them meaningful names
        %Added extra check for 0
    %Version 1.2
        %Changed Phase length so that it is now controlled by the number of
        %cycles instead of just being a number, in case one wanted to
        %change the number of cycles to 4
    Month_Length = 29.530588853;
    if (Cycle_Day >= Month_Length || Cycle_Day <= 0), error('Unnaceptable Cycle Day'); end
    if (Cycle_Day == 0), Moon_Phase = "Unknown";
    else
        n_cycles = 8;
        Phase_Length = Month_Length/n_cycles;
        phases = ["New Moon" "Waxing Crescent" "First Quarter" "Waxing Gibbous" "Full Moon" "Waning Gibbous" "Third Quarter" "Waning Crescent"];
        cnst = ceil(Cycle_Day/Phase_Length);
        Moon_Phase = phases(cnst);
    end
end