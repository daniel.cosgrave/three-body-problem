function [F] = TwoBodyProblem_Equations(vect)
F = zeros(6,1);
F(1:3) = vect(4:6,1);
C = 8.88714886e-10;
F(1:3,1) = vect(4:6,1);
r = norm(vect(1:3,1));
F(4:6,1) = - (C/(r^3))*vect(1:3,1);
end