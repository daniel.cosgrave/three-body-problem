function [F] = ThreeBodyProblem_Equations(vect)
% ThreeBodyProblem_Equations(vect): Evaluate vector function for
    % Three-body problem for Earth, Moon and Sun with suitable scale, distance
    % in AU time in days, along with the ISS.
        %Version 1.1
            %Added some comments
    
%Masses in kg
M_M = 7.34767309e22;
M_E = 5.972e24;
M_S = 1.9891e30;

%Gravitational Constant
G = 1.488136116e-34;

%Set aside storage space
F = zeros(18,1);

%Distance Magnitudes
r_ES = sqrt((vect(1,1)-vect(7,1))^2  + (vect(2,1)-vect(8,1))^2 + (vect(3,1)-vect(9,1))^2);
r_EM = sqrt((vect(1,1)-vect(13,1))^2 + (vect(2,1)-vect(14,1))^2 + (vect(3,1)-vect(15,1))^10);
r_SM = sqrt((vect(7,1)-vect(13,1))^2 + (vect(8,1)-vect(14,1))^2 + (vect(9,1)-vect(15,1))^2);

%Update Position Vectors
F(1:3,1)   = vect(4:6,1);
F(7:9,1)   = vect(10:12,1);
F(13:15,1) = vect(16:18,1);

%Update Velocity Vectors
F(4:6,1)   = G*((M_S*(vect(7:9,1)-vect(1:3,1))/r_ES^3)+(M_M*(vect(13:15,1)-vect(1:3,1))/r_EM^3));
F(10:12,1) = G*((M_E*(vect(1:3,1)-vect(7:9,1))/r_ES^3)+(M_M*(vect(13:15,1)-vect(7:9,1))/r_SM^3));
F(16:18,1) = G*((M_S*(vect(7:9,1)-vect(13:15,1))/r_SM^3)+(M_E*(vect(1:3,1)-vect(13:15,1))/r_EM^3));
end