function [Storage,Experimental_Moon_Phase] = ThreeBodyProblem_RK4(vec0,h,N)
% ThreeBodyProblem_RK4(vec0,h,N): Use fourth order Runge-Kutta
% to solve three-body problem with initial state vector vec0
% Time step = h. Returns the phase of the moon on the day calculated as
% well
% Number of steps taken = N.
    %Version 1.1
        %Added Experimental moon phase
Storage = zeros(length(vec0),N); % assign storage for N time steps of data
v = vec0; % initial conditions
Storage(:,1) = v; % store initial time/position in Storage
Experimental_Moon_Phase = zeros(N,1);
Known_new_moon_day = 2451550.1;
Starting_day = 2458569.5;
Month_Length = 29.53058853;
for count = 2:N
    tv = v; % set temporary variable, tv
    k1 = h*ThreeBodyProblem_Equations(tv);
    tv = v+((1/2)*k1); % update tv
    k2 = h*ThreeBodyProblem_Equations(tv);
    tv = v+((1/2)*k2); % update tv
    k3 = h*ThreeBodyProblem_Equations(tv);
    tv = v+k3; % update tv
    k4 = h *ThreeBodyProblem_Equations(tv);
    v = v + ((1/6)*(k1+(2*k2)+(2*k3)+k4)); % update v
    Storage(:,count) = [v]; % store new position
   
    Experimental_Moon_Phase(count) = ((Known_new_moon_day-(Starting_day+h*count))/Month_Length-floor((Known_new_moon_day-(Starting_day+h*count))/Month_Length))*Month_Length;
end
end